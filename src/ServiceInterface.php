<?php

namespace Drupal\debounce_email;

/**
 * Interface ServiceInterface.
 */
interface ServiceInterface {

  /**
   * Gets the DeBounce Email API key.
   *
   * @return string
   *   The DeBounce Email API key.
   *
   * @throws \Exception
   */
  public function getApiKey();

  /**
   * Validate email.
   *
   * @param string $email
   *   The email address to validate.
   *
   * @return array
   *   The result.
   *
   * @throws \Exception
   */
  public function validateEmail($email);

}
