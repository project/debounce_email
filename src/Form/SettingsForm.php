<?php

namespace Drupal\debounce_email\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The DeBounce Email logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->logger = $container->get('logger.channel.debounce_email');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'debounce_email.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'debounce_email_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('debounce_email.settings');

    $form['is_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable validation'),
      '#default_value' => $config->get('is_enabled'),
      '#description' => $this->t('Enable validation of emails entered in the user registration form with the <a href=":url">DeBounce service</a>.', [
        ':url' => 'https://debounce.io/',
      ]),
    ];

    $form['timeout'] = [
      '#type' => 'number',
      '#min' => 2,
      '#max' => 15,
      '#title' => $this->t('API Timeout'),
      '#description' => $this->t('The maximum number of seconds to wait for a response from the DeBounce API. After this time user registration will proceed without email validation. DeBounce support suggests a maximum wait time of 12 seconds.'),
      '#default_value' => $config->get('timeout'),
    ];

    $form['api_key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('DeBounce <a href=":url">api key</a>', [
        ':url' => 'https://app.debounce.io/api',
      ]),
      '#default_value' => $config->get('api_key'),
    ];

    $form['is_debug_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug mode'),
      '#description' => $this->t('Log every response from DeBounce'),
      '#default_value' => $config->get('is_debug_mode'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('debounce_email.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('is_enabled', $form_state->getValue('is_enabled'))
      ->set('is_debug_mode', $form_state->getValue('is_debug_mode'))
      ->set('timeout', $form_state->getValue('timeout'))
      ->save();
  }

}
