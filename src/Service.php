<?php

namespace Drupal\debounce_email;

use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class Service.
 */
class Service implements ServiceInterface {

  /**
   * The base path to make calls to the DeBounce API.
   */
  const DEBOUNCE_BASE_PATH = 'https://api.debounce.io/v1';

  /**
   * The DeBounce Email logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Drupal\key\KeyRepositoryInterface definition.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * The DeBounce Email configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The Guzzle HTTP Client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a new Service object.
   */
  public function __construct(LoggerChannelInterface $logger_channel_debounce_email, KeyRepositoryInterface $key_repository, ConfigFactoryInterface $config_factory, ClientFactory $httpClientFactory) {
    $this->logger = $logger_channel_debounce_email;
    $this->keyRepository = $key_repository;
    $this->config = $config_factory->get('debounce_email.settings');
    // Limit the number of seconds for a response from DeBounce.
    $this->httpClient = $httpClientFactory->fromOptions([
      'timeout' => $this->config->get('timeout'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getApiKey() {
    $key_id = $this->config->get('api_key');
    if ($key_id) {
      $key_entity = $this->keyRepository->getKey($key_id);
      if ($key_entity) {
        return $key_entity->getKeyValue();
      }
    }
    throw new \Exception('The DeBounce Email API key has not been configured.');
  }

  /**
   * {@inheritdoc}
   */
  public function validateEmail($email) {
    $time = time();
    $response = $this->httpClient->request('GET', self::DEBOUNCE_BASE_PATH, [
      'query' => [
        'api' => $this->getApiKey(),
        'email' => $email,
      ],
    ]);
    $response = json_decode($response->getBody(), TRUE);
    if ($this->config->get('is_debug_mode')) {
      $elapsed_time = time() - $time;
      $this->logger->info('Response in @time seconds from DeBounce: <pre>@response</pre>', [
        '@response' => print_r($response, TRUE),
        '@time' => $elapsed_time,
      ]);
    }
    if (empty($response['success'])) {
      throw new \Exception('The API call to DeBounce failed');
    }
    return $response;
  }

}
