* Introduction

The DeBounce Email module provides validation of emails submitted on the
user registration form by using the DeBounce service:
https://debounce.io/

* Requirements

This module requires the following module:

 - Key (https://www.drupal.org/project/key)

* Installation

Install this module as usual by Composer: composer require drupal/debounce_email

* Configuration

1. Go to https://debounce.io and sign up for a DeBounce account and purchase
  credits.
2. Configure API settings on the DeBounce Email settings page:
  admin/config/services/debounce-email.

* Tip

Add the following to your local settings file to disable validation
$config['debounce_email.settings']['is_enabled'] = FALSE;

* Maintainer

 - @ShaunDychko (https://www.drupal.org/u/shaundychko)
